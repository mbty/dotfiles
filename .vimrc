set number

set tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab
set hlsearch
set showmatch
set colorcolumn=81
set cursorline
set cursorcolumn

call plug#begin()
Plug 'kennykaye/vim-relativity'
Plug 'w0rp/ale'
Plug 'ervandew/supertab'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'romainl/vim-cool'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'let-def/vimbufsync'
Plug 'whonore/coqtail'
Plug 'kien/rainbow_parentheses.vim'
call plug#end()

" let g:localvimrc_sandbox       = 0
" let g:localvimrc_ask           = 0
" let g:ale_cpp_clangtidy_checks = ['-fuchsia-overloaded-operator']
" let g:ale_virtualtext_cursor   = 1

set mouse=a

autocmd VimEnter * AirlineTheme base16_grayscale

" Natural movement on wrapped lines
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap <expr> <Down> v:count ? 'j' : 'gj'
nnoremap <expr> <Up> v:count ? 'k' : 'gk'

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" ALE
let g:ale_linters = { 'cpp': ['g++'], 'c': ['clang'] }
let g:ale_cpp_cc_options = '-Wall -O2 -std=c++20'

" Coqtail
nnoremap <f5> :CoqUndo<CR>
nnoremap <f6> :CoqNext<CR>
nnoremap <f7> :CoqToLine<CR>
nnoremap <f8> :CoqStop<CR>

" Whitespaces
set listchars=tab:->,nbsp:␣,trail:·,precedes:«,extends:»
set list

syntax on
colo darcos

function CoqtailHighlight()
  hi def CoqtailChecked ctermbg=235
  hi def CoqtailSent ctermbg=238
endfunction

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16

au VimEnter * RainbowParenthesesActivate
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
