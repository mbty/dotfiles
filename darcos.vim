hi clear
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="darcos"

hi Normal ctermfg=7
hi LineNr ctermfg=240
hi CursorLineNr ctermfg=15 cterm=none
hi Conditional ctermfg=7
hi Constant ctermfg=166
hi Cursor ctermfg=1 ctermbg=7
hi Debug ctermfg=7
hi Define ctermfg=7
hi Delimiter ctermfg=7

hi DiffAdd ctermbg=7
hi DiffChange ctermfg=7 ctermbg=7
hi DiffDelete ctermfg=7 ctermbg=7
hi DiffText ctermbg=7 cterm=bold

hi Directory ctermfg=7 cterm=bold
hi Error ctermfg=7 ctermbg=1
hi ErrorMsg ctermfg=7 ctermbg=1 cterm=bold
hi Exception ctermfg=7
hi Float ctermfg=7
hi FoldColumn ctermfg=7 ctermbg=7
hi Folded ctermfg=7 ctermbg=7
hi Identifier ctermfg=142 cterm=none
hi Ignore ctermfg=7 ctermbg=7
hi IncSearch ctermfg=7 ctermbg=7

hi keyword ctermfg=7
hi Label ctermfg=7 cterm=none
hi Macro ctermfg=7
hi SpecialKey ctermfg=7

hi MatchParen ctermfg=7 ctermbg=238 cterm=none
hi ModeMsg ctermfg=7
hi MoreMsg ctermfg=7
hi Operator ctermfg=7

" complete menu
hi Pmenu ctermfg=7 ctermbg=234
hi PmenuSel ctermfg=7 ctermbg=235
hi PmenuSbar ctermbg=235
hi PmenuThumb ctermbg=234

hi PreCondit ctermfg=7
hi PreProc ctermfg=160
hi Question ctermfg=7
hi Repeat ctermfg=7
hi Search ctermfg=7 ctermbg=22 cterm=NONE

hi SignColumn ctermfg=1 ctermbg=none
hi SpecialChar ctermfg=7
hi SpecialComment ctermfg=7
hi Special ctermfg=7
if has("spell")
  hi SpellBad ctermfg=3 ctermbg=1
  hi SpellCap ctermfg=3 ctermbg=1
  hi SpellLocal ctermbg=7
  hi SpellRare ctermfg=7 ctermbg=none cterm=reverse
endif
hi Statement ctermfg=7
hi StatusLine ctermfg=7 ctermbg=7
hi StatusLineNC ctermfg=7 ctermbg=7
hi Tag ctermfg=7
hi Title ctermfg=7
hi Todo ctermfg=1 ctermbg=none cterm=bold

hi Type ctermfg=142 cterm=none
hi Underlined ctermfg=1 cterm=none

hi VertSplit ctermfg=7 ctermbg=7 cterm=bold
hi VisualNOS ctermbg=237
hi Visual ctermbg=237
hi WarningMsg ctermfg=7 cterm=bold
hi WildMenu ctermfg=7 ctermbg=7

hi Comment ctermfg=241 cterm=bold
hi NonText ctermfg=1

hi SpecialKey ctermfg=1

hi ColorColumn ctermbg=235
hi CursorLine ctermbg=none cterm=none
hi CursorColumn ctermbg=234
