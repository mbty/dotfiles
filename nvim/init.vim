set number

syntax on

set tabstop=4 softtabstop=4 expandtab shiftwidth=4 smarttab
set hlsearch
set showmatch
set colorcolumn=100

call plug#begin()
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'kennykaye/vim-relativity'
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'w0rp/ale'
"Plug 'tpope/vim-surround'
Plug 'embear/vim-localvimrc'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/seoul256.vim'
Plug 'ervandew/supertab'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'romainl/vim-cool'
Plug 'justinmk/vim-sneak'
Plug 'scrooloose/nerdtree'
Plug 'raimondi/delimitmate'
Plug 'ludovicchabant/vim-gutentags'
Plug 'majutsushi/tagbar'
call plug#end()

let g:localvimrc_sandbox       = 0
let g:localvimrc_ask           = 0
let g:ale_cpp_clangtidy_checks = ['-fuchsia-overloaded-operator']
let g:ale_virtualtext_cursor   = 1
let g:gutentags_cache_dir      = "~/.config/nvim/cache"
nmap <F8> :TagbarToggle<CR>

set mouse=a

"autocmd VimEnter * Limelight
autocmd VimEnter * AirlineTheme angr

autocmd ColorScheme * hi Sneak guifg=black guibg=red ctermfg=black ctermbg=red
autocmd ColorScheme * hi SneakScope guifg=red guibg=yellow ctermfg=red ctermbg=yellow
colo seoul256

" Natural movement on wrapped lines
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap <expr> <Down> v:count ? 'j' : 'gj'
nnoremap <expr> <Up> v:count ? 'k' : 'gk'
