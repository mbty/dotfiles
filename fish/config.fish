function fish_greeting
  clear_and_ls
end

alias cd custom_cd
alias l clear_and_ls
alias ls clear_and_ls
alias m make
alias mkdir custom_mkdir
alias mv custom_mv
alias cp custom_cp
alias rm custom_rm
alias rmdir custom_rmdir
alias s sudo
alias touch custom_touch
alias v custom_vim
alias syu "trizen -Syu --noconfirm"

bind \cl "clear_and_ls; commandline -f repaint"

alias g git
alias gs "git status"
alias gc "git commit"
alias gl "git log"
alias gu "git pull"
alias gp "git push"
alias ga "git add"

function fish_user_key_bindings
  bind \cH backward-kill-path-component
end

function my_signal_handler --on-signal WINCH
  if set -gq is_first_opening;
  else;
    set -g is_first_opening
  end
end

function careful_clear_and_ls
  if test "$status" -eq 0
    clear_and_ls
  end
end

function clear_and_ls
  if test (count $argv) -eq 0
    clear
    exa
  else
    exa $argv
  end
end

function custom_rm
  /usr/bin/rm $argv
  careful_clear_and_ls
end

function custom_mv
  /usr/bin/mv $argv
  careful_clear_and_ls
end

function custom_cp
  /usr/bin/cp $argv
  careful_clear_and_ls
end

function custom_rmdir
  /usr/bin/rmdir $argv
  careful_clear_and_ls
end

function custom_touch
  /usr/bin/touch $argv
  careful_clear_and_ls
end

function custom_cd
  builtin cd $argv
  careful_clear_and_ls
end

function custom_mkdir
  /usr/bin/mkdir $argv
  careful_clear_and_ls
end

function custom_vim
  nvim $argv
  careful_clear_and_ls
end

function mkcd
  mkdir $argv
  cd $argv
end

if status is-login
  if test -z "$DISPLAY" -a $XDG_VTNR = 1
    exec startx --keeptty
  end
end

# opam configuration
source /home/mbty/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
