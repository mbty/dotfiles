set fish_color_autosuggestion black
set fish_color_cancel red
set fish_color_command brwhite bold
set fish_color_comment red
set fish_color_cwd red
set fish_color_cwd_root red
set fish_color_end red
set fish_color_error brred
set fish_color_escape red
set fish_color_history_current red
set fish_color_host_remote red
set fish_color_match red
set fish_color_normal brwhite
set fish_color_operator red
set fish_color_param white
set fish_color_quote yellow
set fish_color_redirection red
set fish_color_search_match red
set fish_color_selection red
set fish_color_stat red
set fish_color_user red
set fish_color_valid_path red
set fish_pager_color_completion black
set fish_pager_color_description red
set fish_pager_color_prefix white
set fish_pager_color_progress red

setenv EXA_COLORS "*=37:di=1;30;01:*.v=1;31:*.pdf=35:*.png=33:*.jpg=33:*.tar.gz=32:CMakeLists.txt=34:cmake_install.cmake=34:Makefile=34:CMakeCache.txt=34:CONTRIBUTING.md=36:LICENSE.md=36:README.md=36:ex=32:*.hh=1;31:*.cc=1;31"

function fish_greeting
  clear_and_ls
end

alias c /home/matthieu/.config/fish/scripts/creat.sh
alias e exit
alias v custom_vim
alias s sudo
alias rb reboot
alias l clear_and_ls
alias sd "shutdown now"
alias syu "trizen -Syu --noconfirm"
alias g git

alias m custom_make
alias cm custom_cmake
alias cd custom_cd
alias t custom_touch
#alias rmdir custom_rmdir
alias rm custom_rm

alias gs "git status"
alias gc "git commit"
alias gl "git log"
alias gu "git pull"
alias gp "git push"
alias ga "git add"

alias ..c "cd ..; clear"

bind \cl "clear_and_ls; commandline -f repaint;"

function fish_user_key_bindings
  bind \cH backward-kill-path-component
end

function my_signal_handler --on-signal WINCH
  #clear_and_ls
  if set -gq is_first_opening;
  else;
    set -g is_first_opening
  end
end

function mkcd
  mkdir $argv
  custom_cd $argv
end

function custom_make
  make -j 8 $argv
end

function custom_cmake
  script -q -c "cmake $argv" >custom_cmake_build_log 2>&1
  less -R custom_cmake_build_log
  rm custom_cmake_build_log
end

function custom_rmdir
  rmdir $argv
  clear_and_ls
end

function custom_rm
  /usr/bin/rm $argv
  if test "$status" -eq 0
    clear_and_ls
  end
end

function custom_cd
  builtin cd $argv
  if test "$status" -eq 0
    clear_and_ls
  end
end

function custom_touch
  touch $argv
  clear_and_ls
end

function custom_vim
  vim $argv
  clear_and_ls
end

function custom_gu
  git pull $argv
  clear_and_ls
end

function custom_gp
  git push $argv
  if test "$status" -eq 0
    clear_and_ls
  end
end

function custom_gc
  git commit $argv
  clear_and_ls
end

function custom_gl
  git log --color=always $argv | less -r
  clear_and_ls
end

function custom_gs
  git status $argv | less
  clear_and_ls
end

function custom_ga
  git add $argv
  clear_and_ls
end

function clear_and_ls
  clear
  exa -s type
end

# opam configuration
source /home/matthieu/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
# ghcup-env
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
test -f /home/matthieu/.ghcup/env ; and set -gx PATH $HOME/.cabal/bin /home/matthieu/.ghcup/bin $PATH
