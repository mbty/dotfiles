set number

syntax off

set tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab
set hlsearch
set showmatch
set colorcolumn=81
colorscheme koehler
filetype plugin indent on
syntax on

call plug#begin()
Plug 'kennykaye/vim-relativity'
Plug 'airblade/vim-gitgutter'
Plug 'w0rp/ale'
Plug 'ervandew/supertab'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'romainl/vim-cool'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/goyo.vim'
Plug 'whonore/Coqtail' | Plug 'let-def/vimbufsync'
call plug#end()

let g:ale_cpp_clangtidy_checks = ['-fuchsia-overloaded-operator']
let g:airline_theme='bubblegum'

set mouse=a

" Natural movement on wrapped lines
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'
nnoremap <expr> <Down> v:count ? 'j' : 'gj'
nnoremap <expr> <Up> v:count ? 'k' : 'gk'

" ALE
let g:ale_linters = {'c': ['clang'], 'cpp': ['clang', 'g++']}
let g:ale_cpp_gcc_options = '-Wall -O2 -std=c++17'
let g:ale_cpp_clang_options = '-Wall -O2 -std=c++17'

" Coqtail
nnoremap <f5> :CoqUndo<CR>
nnoremap <f6> :CoqNext<CR>
nnoremap <f7> :CoqToLine<CR>
nnoremap <f8> :CoqStop<CR>
